﻿
let b1 = document.getElementById("b1");
let b2 = document.getElementById("b2");
let d1 = document.getElementById("train");
let d2 = document.getElementById("answers");

b1.addEventListener("click", () => {
    if (getComputedStyle(d1).visibility != "collapse") {
        d1.style.visibility = "collapse";
    }
    else {
        d1.style.visibility = "visible";
    }
})
var br = document.createElement('br');
document.body.appendChild(br);
b2.addEventListener("click", () => {
    if (getComputedStyle(d2).visibility != "collapse") {
        d2.style.visibility = "collapse";
    }
    else {
        d2.style.visibility = "visible";
    }
})


// prendre le numéro de la lesson
var num = document.getElementsByClassName("num_sheet")[0].textContent;

if (num != 1) {
    var br = document.createElement('br');
    document.body.appendChild(br);
    // Page précédente
    var els = document.createElement('a');
    els.href = (parseInt(num) - 1).toString() + '.html'
    els.style.background = "#fff59d";
    els.style.fontSize = " 20px";
    els.style.color = " darkblue";
    els.style.width = " 150px";
    els.style.margin = "20px";
    els.style.display = 'inline-block';
    els.style.alignContent = "center";
    els.style.padding = "10px";
    els.style.fontStyle = "italic";

    // Ici on crée un contenu
    var contenu = document.createTextNode(parseInt(num) - 1 + ". previous");
    // le contenu et l'élément ne se connaissent pas
    // Il faut ajouter le contenu en tant qu'enfant de l'élément
    els.appendChild(contenu);
    // Maintenant il faut acrocher la branche dans l'arbre
    // On accède au noeud body avec document.body
    document.body.appendChild(els);
}
// Page suivante
if (num != 420){ 
    var els = document.createElement('a');
    els.href = (parseInt(num) + 1).toString() + '.html'
    els.style.background = "#fff59d";
    els.style.fontSize = " 20px";
    els.style.color = " darkblue";
    els.style.width = " 100px";
    els.style.margin = "20px";
    els.style.display = 'inline-block';
    els.style.alignContent = "center";
    els.style.padding = "10px";
    els.style.fontStyle = "italic";
    var contenu = document.createTextNode((parseInt(num) + 1) + ".  next");
    els.appendChild(contenu);
    document.body.appendChild(els);
 }
// ###############################################################################


var train_answers = {
    1: {
        "train_set":  ["parler d'un projet	", "rêver à un voyage	", "Je ne sais rien sur Shakespeare.", "un bon film sur la Chine	", "Il est toujours en colère à propos de quelque chose.	", "Je pense souvent à mes vacances.	", "	Et si on allait à la piscine?	", "	Et si on restait à la maison?	"],
        "answer_set": ["to talk about a plan", "to dream about a journey", "I don't know anything about Shakespeare.", "a good film about China	", "He's always angry about something.		", "	I often think about my holiday.		", "	How about going to the swimming pool? /	What about going to the swimming pool?	", "	How about staying at home?	What about staying at home?	"]
    },
    2: {
        "train_set":  ["environ cinq minutes	", "trois jours, environ	", "à peu prs six mois	", "une année, environ	", "vers six heures	", "environ cent euros	"],
        "answer_set": ["about five minutes", "about three days", "about six months", "about a year /  about one year", "about six o'clock", "about hundred euros /  about one hundred euros", "He's about to emigrate."]
    },
    3: {
        "train_set":  ["Il est sur le point d'émigrer.	", "Elle était sur le point de pleurer.	", "Nous sommes juste sur le point de signer le contrat.	", "Le film est sur le point de commencer.", "Nous étions sur le point d'abandonner.", "Il était juste sur le point de payer."],
        "answer_set": ["He's about to emigrate.", "She was about to cry.", "We are just about to sign the contract.", "The film was about to begin. /  The film was about to start.", "We were about to give up.", "He was just about to pay."]
    },

    4: {
        "train_set": ["Elle a accepté de m'aider.", "Est-ce que tu accepterais de travailler le samedi matin?", "Je n'ai pas accepté de payer pour tout le monde.", "Sa petite amie n'accepte pas qu'il voie d'autres filles."],
        "answer_set": ["She aggreed to help me.", "Would you agree to work on Saturday morning?", "I didn't agree to pay for everybody.", "His girlfriend doesn't let him see other girls.",]
    },

    5: {
        "train_set": ["D'après Camille, Eric va déménager.", "D'après moi, c'est projet très interessant.", "Pour moi, la vie est merveilleuse.", "Selon le times, cette guerre peut être évitée.	"],
        "answer_set": ["According to Camille, Eric is going to move.", "I think, it's a very interesting plan. /  In my opinion, , it's a very interesting plan.", "I think life is marvellous. /  I think life is wonderful. /  In my opinion, life is marvellous.", "According to the times, this war can be avoided."]
    },

    6: {
        "train_set": ["à travers le desert", "à travers la foule", "à travers la rue vide", "a travers la jungle", "à travers le marché	", "à travers le glacier"],
        "answer_set": ["across the desert", "through the crowd", "across the empty street", "through the jungle", "through the market", "across the glacier"]
    },
    7: {
        "train_set": ["une opinion différente", "de hautes montagnes", "une robe longue", "des jeunes gens", "une femme très célèbre", "les autres voitures	", "une chose très importante", "des livres très intéressants", "la Maison Blanche", "Ils sont très riches.", "l'enfant le plus intellligent", "de bons résultats"],
        "answer_set": ["a different opinion", "some high mountains", "a long dress", "young people", "a very famous woman", "the others cars", "a very important thing", "some very interessant books", "the White House", "they're very rich.", "the most intelligent child", "good results"]
    },
    8: {
        "train_set": ["un petit chat blanc", "cet intéressant film allemand", "un liquide chaud et rouge", "de belles chansons espagnoles traditionelles.", "des baskets bleues sales.", "un ravissant vieux village ", "les trois prochaines semaines", "mes deux premières petites amies"],
        "answer_set": ["a small white cat", "this intersting German film", "a warm red liquid", "some beautiful traditional Spanish songs", "dirty blue trainers", "a lovely old village", "the next three weeks", "my first two girlfiends"]
    },
    9: {
        "train_set": ["une petite table ronde", "un tapis vert et noir", "Le temps était froid et déprimant.", "un enfant heureux et confiant", "Son expression (à elle) était froid et énigmatique", "une chaise en métal et en plastique."],
        "answer_set": ["a small round table", "a green and black carpet", "The weather was cold and depressing.", "a happy confident child", "Her expression was cold and enigmatic.", "a metal and plastic chair."]
    },
    10: {
        "train_set": ["pas de test ici"],
        "answer_set": ["no test here"]
    },
    11: {
        "train_set": ["aux long cheveux", "qui joue au football", "qui à l'air stupid", "entre orange et marron", "à un œil", "qui bouge rapidement", "qui à l'air intéressant", "entre gris et noir"],
        "answer_set": ["long-haired", "football-playing", "stupid-looking", "orange-brown", "one-eyed", "fast-moving", "interesting-looking", "grey-black"]
    },
    12: {
        "train_set": ["la chose la plus difficile", "La pauvre!", "un homme riche", "Les riches", "Les égoistes", "les chômeurs", "les malheureux", "les gallois", "Les américains", "Les chinois"],
        "answer_set": ["the most difficult thing", "The poor girl!", "a rich man", "the rich", "selfish people", "the unemployed", "unhappy people", "the Welsh", "American people", "the Chinese"]
    },
    13: {
        "train_set": ["Je veux bien payer pour un peu plus pour la sécurité.", "Cela ne me dérange pas de travailler tard.", "Cela ne me dérange pas si tu restes ici avec nous.", "Cela vous dérangerait d'aider un peu cette dame?", "Ca ne le dérange pas de manquer la fête."],
        "answer_set": ["I don't mind paying a bit more for security. /  I don't object to paying a bit more for security.", "I don't mind working late. /  I don't object to working late.", "I don't mind if you stay here with us. /  I don't oject  you to staying here with us.", "Do you mind helping this lady a little?", "He doesn't mind missing the party."]
    },
    14: {
        "train_set": ["Je n'ai pas peur de l'avenir.	", "Oh! Tu m'as fait peur!", "Est-ce que vous avez peur des cambrioleurs.", "les choses qui nous font peur", "des enfants apeurés"],
        "answer_set": ["I'm not afraid of the future.", "Oh! You frightened me.", "Are you afraid of buglars?", "the things that frighten us", "frightened children"]
    },
    15: {
        "train_set": ["J'ai peur de tomber.", "Est-ce que tu as peur de te faire cambrioler?", "Je n'ai pas peur de dire ce que je pense."],
        "answer_set": ["I'm afraid of falling down.", "Are afraid of being burgled?", "I'm not afraid of telling what I think. /  I'm not afraid to tell what I think.	"]
    },
    16: {
        "train_set": ["Il n'a pas compris, alors j'ai redemandé.", "Pourrais-je réentendre le disque?", "Remet ma montre sur la table, s'il te plaît.", "Si tu le refais, je serai très en colère.", "Il ne rejouera pas.", "J'ai ramené votre bicyclette.", "N'oublie pas d'envoyer les livres.", "Je voudrais revoir les photos."],
        "answer_set": ["He didn't understand, so I asked again.", "Could I hear the disc again.", "Put my watch back on the table, please.", "If you'll do it again, I'll be very very angry.", "He won't play again.", "I've brought your bicycle back.", "Don't forget to send the books back.", "I'd like to see the photos again."]
    },
    17: {
        "train_set": ["Quel âge a ta soeur?", "Elle a 23 ans.", "Vous avez le même âge que sa mère.", "Quand elle avait mon âge ...","Elle a une trentaine d'années.","Il approhe de la cinquantaine.","Je pense qu'elle a dans les 35 ans.","Ils ont une fille de 17 ans."],
        "answer_set": ["How old is your sister?","She's 23. / She's 23 years old.","you are the same age as my mother.","When I was your age ...","She's in her thirties. (30s)","He's in his late forties. (40s)","I think she's in her mid thirties. (30s)","They've got a 17-year-old daughter."]
    },
    18: {
        "train_set": ["Il y a une semaine","Il y a des années","Il y a 2 jours","J'ai vu Robert il y a cinq minutes.","Elle est arrivée il y a une heure.","Je me suis levé il y a longtemps."],
        "answer_set": ["a week ago","year ago","I saw Robert five minutes ago.","She arrived an hour ago.","I got up a long time ago."]
    },
    19: {
        "train_set": ["Elle est toujours d'accord avec tout le monde.","Est-ce qu'il est d'accord avec nous?","- C'est très facile! - Je ne suis pas d'accord!","Je ne suis pas d'acord pour le cadeau."],
        "answer_set": ["She always agrees with everybody.","Does he agree with us?","- It's very easy! - I don't agree!","I don't agree for the present."]
    },
    20: {
        "train_set": ["Tous mes amis sont partis", "Toute la famille a participé à la réunion.", "Toute famille a droit à un logement correct.", "Toute femme comprendra ce problème.", "Toute la ville était inondée.", "J'y vais tous les ans.", "L'hôtel est ouvert toute l'année.", "Je fais du sport tous les jours.", "Je fais du sport tout l'après-midi.", "Elle travaille tout le temps", "Tous les américains ne sont pas des cowboys.","Tout le monde n'aime pas la mer.","Tout n'est pas prêt."],
        "answer_set": ["All my friends are gone.", "All the family particpated to the meeting.", "Every family has the right to proper housing", "Every woman will understand this problem.", "All the city was flooded.", "I go there every year.","The hotel is open all (the) year.","I do sport every day.","I do sport all (the) afternoon.","She works all the times.","Not all Americans are cowboys.","Not every body likes the sea","Not everything is ready."]
    },
    21: {
        "train_set": ["Tout est parfait","Tout son travail est parfait.","J'ai tout oublié.","J'ai oublié tous leurs noms.","Tout est prêt.","Dis-moi tout.","Je te dirais tout ce que je sais.","J'ai tout fini."],
        "answer_set": ["Everything is perfet.","All his(/her) work is perfect","I've forgotten everything.","I've forgotten all their names.","Everything is ready.","Tell me everything.","I'll tell you everything (that) I know.","I've finished everything."]
    },
    22: {
        "train_set": ["Toute la matinée", "L'après-midi entier", "tout une semaine", "sa vie entière", "Nous avons visité tout Londres.","Je ne l'ai pas lu entièrement."],
        "answer_set": ["All the morning","the whole afternoon","a whole week","his(/her/its) whole life", "We visited the whole of London. /  We visited all of London.","I didn't read all of it."]
    },
    23: {
        "train_set": [""],
        "answer_set": [""]
    },
    24: {
        "train_set": ["Epelez à voix haute votre nom, prénom, adresse, num. de téléphone,etc."],
        "answer_set": ["Spell out your name, address, telephone number, etc."]
    },
    25: {
        "train_set": ["Sa femme est une dramaturge connue. Elle écrit aussi des romans.", "- Voulez-vous manger quelque chose ? - Oui, et quelque chose à boire aussi.", "- Alice et Jessica viennent. - Cathy vient-elle aussi ?", "- Les esquimaux vivent au Canada et en Alaska. Ils vivent aussi en Sibérie.", "Il enseigne les maths et aussi l'anglais."],
        "answer_set": ["His wife is a well-known dramastic. She also writes novels.","- Would you like to eat something? - Yes, and something to drink too(/as well).","- Alice and Jessica are comming. - Is Cathy coming as well(/too)?","Eskimos live in Canada and Alaska. They also live in Siberia.","He teaches maths as well as English."]
    },
    26: {
        "train_set": ["Elle parle allemand, et elle peut aussi lire l'espagnol.", "Le temps était mauvais, alors nous sommes restés à la maison.", "Je voudrais une livre de tomates. J'ai aussi besoin de pommes de terre.", "- Tout le monde est parti. - Alors pourquoi es-tu là ?", "Il travaille dur, alors il est très fatigué.", "Elle aime la musique et la peinture. Elle va aussi très souvent danser."],
        "answer_set": ["She speaks German, and she can also read Spanish.","The weather was bad, so we stayed at home.","I'd like a pound of tomatoes. I also need some potatoes.","- Everybody's gone. - So why are you here?","He works hard, so he gets very tired.","She likes music and painting. She also goes dancing very often."]
    },
    27: {
        "train_set": ["Bien que je l'apprécie, il est un peu étrange.", "Il est très amical, quand même.", "Je ne sais pas parler espagnol, bien que je puisse le lire assez bien.", "Elle ne mangait rien, même si elle avait faim.", "Tu as l'air d'avoir besoin d'aide.", "Nous sommes allés nager malgré le froid"],
        "answer_set": ["Although(/Though) I like him, he's a bit strange.","He's very friendly, though","I can't speak spanish, although(/though) I can read it quite well.","She didn't eat anything, even though she was hungry.","You look as though you need help.","We went swimming in spite of the cold"]
    },
    28: {
        "train_set": ["Il perd tout le temps ses lunettes.","J'oublie tout le temps mes clés.","Elle rit tout le temps.","Nous perdions constamment notre chemin."],
        "answer_set": ["He's always losing his glass","I'm always forgetting my keys.","She's always laughing.","We were always losing our way."]
    },
    29: {
        "train_set": ["Essaie de comprendre.","Allons voir Hassan.","Viens déjeuner ave nous demain.","Je veux aller voir un film"],
        "answer_set": ["Try and understand.","Let's go and see Hassan.","Come and have lunch with us tomorrow.","I want to go and see a film."]
    },
    30: {
        "train_set": ["mes amis et ma famille","la maison et le jardin","en Angleterre et en Ecosse","Je chante et joue de la guitare.","Il dort ou il est sourd.","Tu veux du vin ou de la bière?"],
        "answer_set": ["my friends and familly","the house and the garden","in England and Sotland","I sing and play guitare","He's asleep or deaf.","Do you want some wine or beer?"]
    },
    31: {
        "train_set": ["pas de test ici"],
        "answer_set": ["no test here"]
    },
    32: {
        "train_set": ["pas de test ici"],
        "answer_set": ["no test here"]
    },
    33: {
        "train_set": ["un autre moreau de pain","Est-e que je peux en avoir un autre?","J'ai besoin de quelques minutes de plus.","C'était dans un autre pays.","Est-ce que tu peux rester encore deux jours?","Pouvez-vous me donner un autre verre, s'il vous plaît?"],
        "answer_set": ["another piece of bread","May(/can) I have another?","I need another few minutes.","It was in another country.","Can you stay another two days?","Can you give me another glass, please?"]
    },
    34: {
        "train_set": ["- Quand voulez-vous venir ? Oh, n'importe quand.", "- Où aimeriez-vous vivre ? - N'importe où, mais pas ici.", "Elle ne sort avec tout le monde, elle est snob.", "Notre chien mange tout ce que vous lui donnez."],
        "answer_set": ["- When would you like to ome? -Oh, any time.","- Where would you like to live? - Anywhere but not here.","She doesn't go out with anybody(/anyone), she's snobbish.","Our dog will eat anything you give him."]
    },
    35: {
        "train_set": ["Vous n'avez pas l'air beaucoup plus vieux qu'avant.", "Pensez-vous que le temps va s'améliorer un peu?", "Cette école n'est pas différente de la précédente.", "Vous devez être à la maison  au plus tard à 10 heures.", "J'ai peur que le film ne n'était pas bon du tout.", "Est-ce que ça sert à quelque chose de demander à Mary d'aider?"],
        "answer_set": ["You don't look any older than before."," Do you think the weather's going to get any better?","This school ins't any different from the last one.","You must be home no later than 10 o'clock.","I'm afraid the film was no good.","Is it any use asking Mary to help?"]
    },
    36: {
        "train_set": ["Je vais apprendre le piano.","Qui t'a appris à jouer au rugby?","Apprend-moi une chanson.","Je t'apprendrai à conduire.","Je viens d'apprendre qu'ils se marient.","J'ai une bonne nouvelle à t'apprendre.","Je viens d'apprendre la naissance de votre fils."],
        "answer_set": ["I'm going to learn the piano.","Who taugh you to play rugby?","Teach me a song.","I'll teach you to drive.","I've just heard that they are getting married.","I've got some good news to tell you.","I've heard about the birth of your son."]
    },
    37: {
        "train_set": ["Nous avons vu un film et sommes allés boire un verre après.", "Ils ont vécu à Edimbourg après leur mariage.", "Le jour après demain.", "Je dois travailler maintenant, mais je serai libre après pendant quelques heures.", "J'ai écrit quelques lettres, et après j'ai pris un bain.", "Des années plus tard, j'ai compris ce qui s'était réellement passé."],
        "answer_set": ["We saw a film and went out for a drink afterwards(/after that)", "They lived in Edinburgh after their marriage.", "The day after tomorrow.", "I have to work now, but I'll be free  afterwards(/after that) for a few hours.","I wrote some letters, and then  (/afterwards /after that) I had a bath.","Years after, I understood what had really happened."]
    },
    38: {
        "train_set": ["Elle est arrivé à cinq heures.","A quelle heure êtes-vous arrivé à l'hôtel?","On arrivera à Los Angles demain soir.","Je n'arrive pas à les comprendre.","Sais-tu ce qui est arrivé hier?","- Où est Jennifer? - Ell arive.","Ca t'arrive d'oublié ton numéro de téléphone?","Qu'est ce qui leur arrive?"],
        "answer_set": ["She arrived at five.", "What time did you arrive at the hotel?", "We'll arrive at Los Angleles tomorrow evening.", "I can't manage to understand them.", "Do you know what happened yesterday?", "- Where's Jennifer? - She's coming.", "Do you ever forget you (tele)phone number?","What's the matter with them?"]
    },
    39: { "train_set": [""], "answer_set": [""] },
    40: { "train_set": [""], "answer_set": [""] },
    41: { "train_set": [""], "answer_set": [""] },
    42: { "train_set": [""], "answer_set": [""] },
    43: { "train_set": [""], "answer_set": [""] },
    44: {
        "train_set": ["Elle est aussi intelligente que sa soeur.","Je suis moins fatigué qu'hier.","Nous sommes venus le plus vite possible.","Il fait deux fois plus froid que ce matin","Je ne travaille pas aussi bien que toi.","Je sors moins souvent qu'autrefois."],
        "answer_set": ["She's as intelligent as her sister.","I'm not as tired as yesterday. / I'm not so tired as yesterday.","We came as quickly as possible. /  I'm came as fast as possible.","It's twice as cold as this morning.","I don't work as well as you. /  I don't work so well as you.","I don't go out as often as I used to. /  I don't go out so often as I used to."]
    },
    45: {
        "train_set": ["Il n'y a pas beaucoup de gens comme toi.", "J'aime manger du porc avec de la sauce aux pommes, comme en Angleterre.", "Tu ressembles à ton père.", "Je peux résister à tout sauf à la tentation, comme l'a dit Oscar Wild.", "J'aimerais traverser l'Afrique en ballon, comme dans l'histoire de Jule Verne.", "Mon ami Frank est comme un frère pour moi.", "J'aime la mer, été comme hiver.", "Je vais travailler comme guide touristique pendant l'été."],
        "answer_set": ["There aren't many people like you.", "I like to eat pork with apple sauce, as they do in England.", "You loook like your father.", "I can resist anything except temptation, as Oscar Wild said.", "I'd like to cross Africa in a balloon, as they did in Jule Verne's story.", "My friend Frank is like a brother to me.", "I love the sea, in summer as in winter.", "I'm going to work as a tourist guide during the summer."]
    },
    46: {
        "train_set": ["pas de test ici"],
        "answer_set": ["no test here"]
    },
    47: {
        "train_set": ["Il a l'air d'avoir faim.","J'ai l'impression de rêver.","On dirait qu'elle ne comprend pas.","Il avait l'air de réfléchir.","On dirait que vous avez froid.","J'avais l'impression d'être seul au monde."],
        "answer_set": ["He looks as if he's hungry. /  He looks as though he's hungry.","I feel as if I'm dreaming. /  I feel as though I'm dreaming.","She looks(/seems) as if she doesn't understand. /  She looks(/seems as tough she doesn't understand.","He looked(/seemed) as if he was thinking. /  He looked(/seemed) as though he was thinking.","You look as if you're cold; /  You look as though you're cold.","I felt as if I was alone in the world; /  I felt as though I was alone in the world."]
    },
    48: {
        "train_set": ["Je resterai à la campagne tant qu'il fera beau.","Nous viendrons ce soir à condition que tu invites Maria.","Tu peux manger avec nous à condition de nous aider.","Du moment que j'ai des amis, je suis heureux.","Tant que tu n'auras pas téléphoné, il ne saura rien.","Vous pouvez y aller pourvu que vous soyez de retour à 9 heures."],
        "answer_set": ["I'll stay in the country as long as it's fine.","We'll come this evening as(/so) long as you invited Maria. /  We'll comme this evening provide (that) you invite Maria.","You can eat with us as(/so) long as you help us. /  You can eat with us provided (that) you help us.","As(/so)  long as I have friends, I'm happy.","Until you've (tele)phoned, He won't know anythng.","You can go (there) provided (that) you're back at nine (o'clock)."]
    },
    49: {
        "train_set": ["Je n'ai pas autant de temps libre que l'année dernière.", "Il y a autant de restaurant japonais à Londres qu'à Paris.", "Vous pouvez manger autant que vous voulez pour 12 €.", "- Est-ce que tu as beaucoup de cousins? - Pas autant que toi", "Il n'y a pas autant d'essence que je croyais.","J'ai deux fois plus d'amis que ma soeur.","Il me faut le plus possible d'argent.","J'ai invité le plus possible de gens."],
        "answer_set": ["I haven't got as(/so) much free time as last year.","There ar as many Japonese restaurants in London as in Paris.","You can eat as much as you want(/like) for €12.","- Have you got many cousins? - Not as(/so) many as you.","There isn't as(/so) much petrol as I thought.","I've got twice as many friends as my sister.","I ('ve) invited as many people as possible."]
    },
    50: {
        "train_set": ["J'ai demandé son nom.", "Il m'a demandé de l'argent.", "Je demanderai un rendez-vous avec Mr Brown.", "Elle m'a demandé un verre de vin.", "Nous avons demandé le chemin de l'église.", "Demande à Harry.", "Je demanderai à Rebecca de venir.","Peux-tu demander à Gabriel son adresse?","J'ai demandé l'heure à mon voisin.","Demande le chemin à un agent de police.","Il demanda de l'aide à mon père."],
        "answer_set": ["I asked his name.","He asked me for money.","I'll ask for an appointment with Mr Brown.","she asked me for a glass of wine.","We asked the way to the church.","Ask Harry.","I'll ask Rebecca to come.","Cans you ask Gabriel his address?","I asked my neighbour the time.","Ask a policeman the way.","He asked my father for help."]
    },
    51: { 
        "train_set": ["J'ai sommeil.","Est-ce que tu dors?","Je me suis endormi à trois heures.","La femme était endormie.","une femme endormie"],
        "answer_set": ["I'm sleepy","Are you asleep?","I fell asleep at three o'clock.","The woman was alseep.","a sleeping woman"]
    },
    52: { 
        "train_set": ["J'habite au 62, Hight Street", "C'est à 800 mètres d'ici. (1/2 mile)", "J'ai un appartement au deuxième étage.", "Nous partons pour New York par le premier avion demain.", "J'étais à l'université de 1995 à 1998", "Ma soeur habite dans George Street.", "Il y a un mot que je ne comprends pas à la page 25.", "C'est à la ligne 13.", "J'aime marcher sous la pluie.", "Elle arrive par le prochain train."],
        "answer_set": ["I live at 62, High Street","It's half a mile from here","I've got a flat on the second floor.","We've leaving for New York on the first plane tomorrow.","I was at university from 1995 to 1998","My sister lives in George Street.","There's a word that I don't understand in page 25.","It's in line 13.","I like walking in the rain.","She's arriving on the next train."]
    },
    53: {
        "train_set": ["Je serai libre à quatre heures.", "Je n'aime pas me lever le matin.", "Que faites-vous samedi soir ?", "Nous allons au Portugal à Pâques.", "Je suis né en août.", "Je suis toujours occupé le week-end.", "Nos voisins font beaucoup de bruit la nuit.", "Mon anniversaire est le 21 mars.", "A quelle heure serez-vous prêt ?", "Shakespeare est né au 16ème siècle."],
        "answer_set": ["I'll be free at four o'clock.","I don't like getting up in the morning.","What are you doing on Saturday evening?","We're going to Portugal in Easter.","I was born in August.","I'm always busy at weekends.","Our neighbours make a lot of noise at night.","My birthday is on March 21st.","What time will you be ready?","Shakespeare was born in the 16th century."]
    },
    54: {
        "train_set": ["J'ai passé la soirée chez Peter", "Pourquoi ne viendrais-tu pas chez nous le week-end prochain ?", "Peux-tu apporter des disques à la fête ?", "Il est temps de rentrer à la maison.", "Elle est à l'hôpital depuis deux semaines.", "D'habitude, je vais au travail en voiture le matin.", "A quelle heure arrivons-nous à l'aéroport ?", "Arrête de jeter des pierres sur ta soeur.", "Mes voisins crient toujours sur leurs enfants.", "Je vais à Heidelberg pour étudier l'allemand.", , "Je vais étudier l'allemand à Heidelberg."],
        "answer_set": ["I spent the evening at Peter's", "Why don't you come to our house next weekend?", "Can you bring some discs to the party?", "It's time to go home.", "She's been in hospital for two weeks.", "I usually drive to work in the morning.", "What time do we arrive at the airport?", "Stop throwing stones at you sister.", "My neighbours are always shouting at their children.", "I'm going to Heidelberg to study German.", "I'm going to study German in(/at) Heidelberg."]
    },
    55: {
        "train_set": ["Au début, je pensais que c'était sa soeur, mais ensuite j'ai réalisé que c'était sa femme.", "Je me souviendrai toujours du jour où je t'ai vu pour la première fois.", "Je veux d'abord parler de nos projets, puis nous discuterons du coût.", "Je ne l'aimais pas au début, mais plus tard nous sommes devenus de bons amis.", "Je me demande qui arrivera en premier.", "Au début, je pensais que l'anglais était difficile, mais maintenant ça va."],
        "answer_set": ["At first I tought she was his sister, but then I realised she was his wife.","i will always remember the day when I first I saw you.","First I want to talk about our plans, then we'll discuss the cost.","I didn't like him at first, but later we became good friends.","I wonder who will arrive first.","At first I though English was hard, but now it's OK."]
    },
    56: {
        "train_set": ["J'aime le jazz. J'aime aussi la musique classique.", "Je suis désolé que vous soyez si malheureuse.", "Londres est aussi loin qu'Amsterdam.", "Il faisait beau, alors elle a décidé de se promener.", "Il est très beau(/belle apparence), et c'est aussi une personne sympathique.", "Je parle aussi mal l'allemand que l'anglais."],
        "answer_set": ["I like Jazz. I also like classical music.","I'm sorry you're so unhappy.","London is as far away as Amsterdam.","The weather was fine, so she decided to go for a walk.","He's very good-looking, and he's also a nice person.","I speak German as badly as English."]
    },
    57: {
        "train_set": ["- Je m'ennuie. - Moi aussi.", "- J'aime ne rien faire. - Moi aussi.", "- Je n'ai pas encore pris mon petit-déjeuner. - Moi non plus.", "- Je n'aime pas son attitude. - Moi non plus."],
        "answer_set": ["- I'm bored. - So am I.","- I like doing nothing. - So do I.","- I haven't had breakfast yet. - Neither have I.","- I don't like her attitude. - Neither do I."]
    },
    58: {
        "train_set": ["avant Pâques","J'ai téléphoné avant, mais il n'y avait personne.","Je veux faire le tour du monde, mais avant il faut que je trouve de l'argent.","Je pars ce soir. Est-ce qu'on peut se voir  avant?","Avant, je voyageais beaucoup, mais je n'en ai plus envie.","Avant, elle était très timide, mais elle a changé."],
        "answer_set": ["Before Easter", "I phoned before, but there was no one there", "I want to go around the world, but I have to find money first(/befor that).", "I'm leaving tonight. Can we meet first? /  Can we meet before that?", "I used to travel a lot, but I don't feel like it anymore.", "She used to be very shy, but she has changed"]
    },
    59: {
        "train_set": ["Nous sommes en retard.", "Il n'est pas riche.", "Est-ce qu'elles sont libres demain?", "N'êtes-vous pas Bob Wilson?", "Nous étions jeunes.", "Je n'ai pas été surpris.", "Tu étais triste hier.", "Est-ce qu'il sera là?", "Je ne serai pas avec toi.", "Je serais contente si je gagnais", "J'ai raison, n'est-ce pas ?", "Ça me fera plaisir que tu viennes.", "Ça lui ferait plaisir si tu venais.", "Tu étais chez toi hier soir ?", "Il a toujours été paresseux.", "Je ne suis jamais allé au Canada.", "Qu'aurais-tu fait si tu y avais été ?", "Ne sois pas en retard !", "Je n'aime pas qu'on me regarde.", "Arrête ça ! Tu es stupide !"],
        "answer_set": ["We're late.", "He's not rich.", "Are they free tomorrow?", "Aren't you Bob Wilson?", "We were young.", "I wasn't surprised.", "You were sad yesterday.", "Will he be be there?", "I won't be with you.", "I'd be happy if I won", "I'm right, aren't I?", "I'll be pleased if you come.", "She'd be pleased if you came.", "Were you at home last night?", "He has always been lazy.", "I 've never been to Canada.", "What would you have done if you had been there?", "Don't be late!", "I don't like being looked at.", "Stop it! You're being stupid!"]
    },
    60: {
        "train_set": ["Demain le président inaugurera un nouvel hôpital à New York.","Ce jour-là, j'ai vu pour la première fois la maison où nous devions passer dix ans de notre vie.","Vous ne devez pas lire mes lettres.","Tu dis merci à papa.","Vous prendrez le train de 8h 15.","Les enfants ne doivent pas jouer avec ma caîne stéréo.","Ce sirop est à prendre tous les soirs. ","Les soldes doivent comencer la semaine prochaine."],
        "answer_set": ["Tomorrow the President is to open a new hospital in New York.","That day I saw for the first time the house where we were to spend ten years of our lives.","You are not to read my letters.","You are to say thank you to your Daddy.","You are to take the train at 8.15.","The children aren't to play with my stereo.","This syrup is to be taken every evening.","The sales are to start next week."]
    },
    61: {
        "train_set": ["Elle est américaine."," Elle a deux enfants.","Avez-vous soif?","J'ai trop de travail.","tu es fatigué.","Je n'ai pas peur de vous.","Vous avez tort.","Est-ce que vous avez mon adresse?","Le film est mauvais.","Nous avons des amis en Ecosse.","Ils sont très sympatiques.","Mes parents ont tous les deux quarante ans.","Vous êtes sûr?","Je n'ai aucune idée.","Ma chambre n'a que deux mètres de large.","J'ai sommeil.","Il est parti.","J'étais tombé.","Nous sommes arrivés.","Pourquoi êtes-vous venu?"],
        "answer_set": ["She is amaerican.", "She's got two children.", "Are you thirsty?", "I have too much work.", "You're tired.", "I'm not afraid of you.", "You're wrong.","Have you got my address?","This film is bad.","We have friends in Scotland.","They're very nice.","My parents are both forty.","Are you sure?","I have no idea.","My room is only two metres wide.","I'm sleepy.","He has left(/gone).","I had fallen.","We've arrived.","why have you come?"]
    },
    62: {
        "train_set": ["Il y a des gens qui sont capables de marcher sur les mains.","Je n'ai jamais su faire la cuisine.","Je saurai bientôt parler anglais couramment.","Dans cent ans, les gens pourront voyager partout.","Il est utile de savoir conduire;.","Nous ne sommes pas en mesure de vous prêter de l'argent."],
        "answer_set": ["Some people are able to walk on their hands.","I've never been able to cook.","I'll soon be able to speak english fluently.","In a hundred years, people will be able to travel everywhere.","It's useful to be able to drive.","We are unable to lend you (any) money."]
    },
    63: {
        "train_set": ["Nous n'avons pas le droit de choisir.","Vous n'avez pas le droit de marcher sur l'herbe.","Est-ce que tu as le droit de sortir en semaine?","Il est interdit de stationner ici, Madame.","Nous n'avons pas le droit de juger les autres."],
        "answer_set": ["We aren't allowed to choose.","You're not allowed to walk on the grass.","Are you allowed to go out on weekdays?","Parking here isn't allowed, madam.","We don't have the right to judge others."]
    },
    64: {
        "train_set": ["Ma soeur et moi sommes nés en mars", "Oliver est né le jour de Noël", "Trois bébés sont nés dans notre village la semaine dernière", "Où êtes-vous né ?", "Savez-vous combien de bébés naissent chaque minute dans le monde ?", "Son bébé va naître dans quelques semaines."],
        "answer_set": ["Both my sister and I were born in March.","Oliver was born on Christmas Day.","Three babies were born in our village last week.","Where were you born?","Do you know how many babies are born every minute in the world?","Her baby will be born in a few weeks."]
    },
    65: {
        "train_set": ["Tu n'es pas censé le savoir.","Je dois être à Paris dans une heure, je n'y arriverai jamais."],
        "answer_set": ["You're not supposed to know.","I'm supposed to be in Paris in an hour, I'll never make it."]
    },
    66: {
        "train_set": ["parce que j'étais fatigué", "à cause des examens", "à cause des élections", "parce que nous n'avions pas d'argent", "parce que je parle japonais", "à cause de ma mauvaise prononciation"],
        "answer_set": ["because I was tired","because of the exams","because of the elections","because we didn't have any money","because I speak Japonese","because of my bad pronunciation"]
    },
    67: {
        "train_set": ["Mes frèes vont tous les deux à l'université.","Les deux fenêtres sont cassées.","Il est à la fois intelligent et sensible.","Mes deux poches sont déchirées.","Je joue au football et au rugby.","Les deux voitures ne sont pas pareilles."],
        "answer_set": ["Both my brothers go to university.","Both the windows are broken. /  The windows are both broken.","He's both intelligent and sensitive.","Both my pocket are torn.","i play both football and rugby.","The two cars aren't the same."]
    },
    68: {
        "train_set": ["Tu peux m'apporter une assiette, s'il te plaît ?", "J'emmène Joanne à Londres demain.", "On a emmené les enfants au cirque la semaine dernière.", "J'ai ramené du whisky d'Ecosse l'été dernier.", "Porte cette lettre à la poste.", "Si tu ne veux pas rentrer à pied, je t'emmène en voiture."],
        "answer_set": ["Could you bring me a plate, please?","I'll take Joanne to London with me tomorrow.","We took the children to the circus last week.","I brought back some whisky from Scotland last summer.","Take this letter to the post office.","I you don't want to walk home, i'll take you in my car."]
    },
    69: {
        "train_set": ["pas de test ici"],
        "answer_set": ["no test here"]
    },
    70: {
        "train_set": ["Tout le monde parlais sauf Jack.","Elle ne mange que des glaces.","Tachid habite deux maisons plus loin.","Ce lire était son avant-dernier."],
        "answer_set": ["Everybody but Jack was talking. /  Everybody was talking but Jack.","She eats nothing but ice-cream(s).","Rachid lives next door but one.","This book was his(/her) last but one."]
    },
    71: {
        "train_set": ["Je dois être rentré pour huit heures.","Pouvez-vous le réparer d'ici mercredi?","Je vous appellerai à dix heures au plus tard","J'aurai l'argent d'ici Noël.","Le temps qu'il arrive, j'étais parti.","D'ici à ce que tu reçoives cette lettre, je serai marié."],
        "answer_set": ["I have to be home by eight o'clock.", "Can you fix it by Wednesday.", "I'll call you by ten o'clock.", "I'll have the money by Christmas.", "By the time he arrived, I was gone.", "By the time you get this letter, I'll be married."]
    },
    72: {
        "train_set": ["Il peut attendre.","Elle ne peut pas venir.","Est-ce que vous savez nager?","j'aimerais savoir parler allemand.","Je pourrai payer demain.","j'ai toujours pu m'entendre facilement avec les gens.","Il ne pouvait pas comprendre.","Pourriez-vous m'aider un instant?"],
        "answer_set": ["he can wait.","She cannot(/can't) come.","Can you swim?","I'd like to be able to speak German.","I'll be able to pay tomorrow.","I've always been able to get on easily with people.","He couldn't understand.","Could you help me for a moment?"]
    },
    73: {
        "train_set": ["Désolé, je ne peux pas.","Il ne sait pas danser.","Je savais déjà lire à l'âge de trois ans.","Je ne pouvais rien faire.","Pourriez-vous me montrer des pulls, s'il vous plaît?","Tu pourrais me dire la vérité, pour une fois !"],
        "answer_set": ["Sorry, I can't. / Sorry, i'm afraid I can't.","He can't dance.","I could (already) read when I was three.","I couldn't do anything. /  I could do nothing.","Could you show me some pullovers, please?","You could tell me the truth for once!"]
    },
    74: {
        "train_set": ["Je peux prendre un gâteau?","Vous pouvez arrêter maintenant.","Est-ce que je pourrais avoir un kilo de pommes, s'il vous plaît?","Pourrais-je vous demander quelque chose?","Tu peux m'aider, si tu veux.","Quand j'avais 15 ans, je pouvais partir en vacances sans mes parents."],
        "answer_set": ["Can I take a cacke.","You can stop now.","Could I have a kilo of apples, please?","Could I ask you something?","You can help me if you like.","When I was 15, I could go on holiday without my parents."]
    },
    75: {
        "train_set": ["Tu entends la pluie?","Qu'est ce que tu vois?","Après la piqûre, je ne sentais rien.","Je vois Nadia dans la rue.","Le chien entend quelque chose.","Je sentais sa respiration sur ma figure."],
        "answer_set": ["Can you hear the rain?","What can you see?","After the injection, I couldn't feel anything.","I can see Nadia in the street.","The dog can hear something.","I could feel his(/her) breath on my face."]
    },
    76: {
        "train_set": ["Je chantais comme un ange quand j'étais petite.", "Ma moto est tombée en panne, mais j'ai réussi à la réparer moi-même", "J'avais un problème difficile, mais j'ai réussi à trouver une solution.", "Shakespeare parlait très bien l'italien.", "Après trois heures, nous avons réussi à atteindre le sommet de la montagne.", "Ma mère jouait très bien du piano quand elle était jeune."],
        "answer_set": ["I could sing like an anglel when I was small.","My bike broke down, but I managed to repair it myself. /  but I was able to repair it myself.","I was a difficult problem, but I managed(/was able) to find a solution.","Shakespeare could speak very good Italian.","After three hours we managed(/were able) to reach the top of the mountain.","My mother could play the piano very well when she was young."]
    },
    77: {
        "train_set": ["Mark n'a pas pu prendre ma voiture ! Qui a pu la prendre?","J'aurais pu te voir hier.","Tu aurais pu m'aider.","Il aurait pu aller plus vite.","J'aurais pu étonner tout le monde.","J'aurais pu mourir à cause de vous.","Vous auriez pu me dire que j'aurais tout ce travail à faire juste avant Noël !"],
        "answer_set": ["Mark can't have taken my car! Wha can have taken it?","I could have seen you yesterday.","You could have helped me.","He could have gone faster. /  He could have gone more quickly.","I could have surprised everybody.","I could have died because of you.","You could have told me (that) I'd have all this work to do just before Christmas!"]
    },
    78: {
        "train_set": ["- Qu'est-ce que vous cherchez? - Mes lunettes.","Pouvez-vous aller cherher Ben à l'aéroport à midi?","Je vais chercher des livres.","Nous avons cherché un petit appartement.","Je viendrai te chercher demain matin.","Va chercher du pain, s'il te plaît."],
        "answer_set": ["- What are you looking for? - My glasses","Can you fetch (/get)(/pick up) Ben from the airport at twelve (/o'clock) (at midday)?","I'm going to get some books.","We're looking for a small flat.","I'll pick you up tomorrow morning.","Feth (/get) some bread, please."]
    },
    79: {
        "train_set": ["Je crois qu'elle est chez le boucher.", "On a dîné chez Anne hier soir.", "Demande-lui de venir chez moi.", "Tu veux rentrer ?", "Il étudie la critique sociale chez Balzac.", "Elle a écrit un livre sur les coutumes de mariage chez les Inuits."],
        "answer_set": ["I think she's at the butcher's.","We had dinner at Anne's last nght.","Ask him to come to my place.","do you want to go home?","He's studying social criticism in Balzac.","She wrote a book about marriage customs among the Inuits."]
    },
    80: {
        "train_set": ["plus fin que, le plus fin", "plus plein que, le plus plein", "plus drôle que, le plus drôle", "plus fin que, le plus fin", "plus jeune que, le plus jeune", "plus chaud que, le plus chaud", "plus court que, le plus court", "plus idiot que, le plus idiot", "plus beau que, le plus beau", "plus intelligent que", "le plus intéressant", "le plus long", "le plus drôle", "plus dur que", "le plus dur", "le plus paresseux", "plus paresseux que", "L'histoire est plus intéressante que la géographie.", "Le printemps est la plus agréable saison de l'année.", "Je pense que la chose la plus importante dans la vie est d'être heureux.", "La biologie est plus facile que les maths."],
        "answer_set": ["finer than, the finest", "fuller than, the fullest", "funnier than, the funniest", "thinner than, the thinnest", "younger than, the youngest", "hotter than, the hottest", "shorter than, the shortest", "sillier than, the silliest", "more beautiful than, the most beautiful", "smarter than", "the most interesting", "the longest", "the funniest", "harder than","the hardest", "the laziest","lazier than", "History is more interesting than geography.","Spring is the nicest season of the year.","I think the most important thing in life is to be happy.","Biology is easier than maths."]
    },
    81: {
        "train_set": [" plus dur", "plus fort (son)", " plus rapidement", " plus facilement", " plus tard", "plus légèrement"],
        "answer_set": ["harder","louder","more quickly","more easily","later","more lightly"]
    },
    82: {
        "train_set": ["Mon meilleur ami vient de décider d'aller au Canada.", "- Le film d'hier soir n'était pas mal - Mieux que ce que j'attendais", "La pire expérience de ma vie a été un accident de voiture l'année dernière.", "Cet hiver est encore pire que l'hiver dernier.", "Tout le monde parle bien l'anglais, mais c'est Mary qui parle le mieux. ", "Nous chantons tous mal, mais c'est moi qui chante le plus mal.", "Voulez-vous d'autres informations ?", "Il a six enfants, ils sont tous à l'école sauf l'aîné", "La planète la plus éloignée du soleil s'appelle Pluton.", "Leur fils aîné travaille dans une banque, et l'autre est médecin."],
        "answer_set": ["My best friend has just decided to go to Canada.","- The film last night wasn't bad - Better than I expected","The worst experience of my life was a car accident last year.","This winter is even worse than last winter.","Everybody speaks English well, but Mary speaks best.","We all sing badly, but I sing worst of all.","Would you like any further information?","He's got six children, they're all at school except the eldest","The farthest(/furthest) planet from the sun is called Pluto.","Their elder(/eldest) son works in a bank, and the other is a doctor."]
    },
    83: {
        "train_set": ["Cet hôtel est moins cher que l'autre.", "J'ai écrit moins que toi.", "Je dors moins d'heures qu'avant.", "J'ai moins d'argent que je ne le pensais.", "Eva est la fille la moins compliquée que je connaisse.", "J'ai passé le moins de temps possible à la maison.", "C'est le genre de film que j'aime le moins.", "Du moins, c'était mon opinion."],
        "answer_set": ["This hotel is less expensive than the other.","I've written less than you.","I sleep fewer hours than I used to.","I've got less money than I hought to.","Eva is the least complicated girl I know.","I spent as little time as possible at home.","It's the sort of film I like least.","At least, it was my opinion."]
    },
    84: {
        "train_set": ["beaucoup plus froid","encore plus froid","beaucoup plus intéressant","beaucoup mieux","encore plus vite","encore plus cher"],
        "answer_set": ["much colder","even colder","much more intersting","much better","even faster /  even more quickly","even more expensive"]
    },
    85: {
        "train_set": ["Il est plus petit que moi.","J'ai plus d'amis qu'elle.","Ils sont plus heureux que nous.","J'ai plus de vacances que vous."],
        "answer_set": ["He's smaller than me. /  He's smaller than I am.","I've got more friends than her(/than she has)","They're happier than us(than we are).","I've more holiday than you (have)."]
    },
    86: {
        "train_set": ["La vie devient de plus en plus difficile.","Il fait de plus en plus chaud.","J'ai de plus en plus de travail.","Cet exercice est de plus en plus ennuyeux.","Nous avons de moins en moins d'argent.","Il y a de moins en moins d'espaces verts."],
        "answer_set": ["Life is getting more and more difficult.","It's getting warmer and warmer.","I've got more and more work.","This exercise is getting more and more boring.","Whe have less and less money.","Ther are fewer and fewer open spaces."]
    },
    87: {
        "train_set": ["Plus je dors, plus je me sens fatigué.","Plus je lis de livres, plus j'oublie.","Plus vous montez haut, plus c'est dangereux.","Plus j'achète de chaussures, plus j'ai envie d'en acheter.","Plus j'écoute, moins je comprends.","Moins je la vois, moins j'ai envie de la voir.","Jétais d'autant plus étonné qu'il n'a rien dit.","Elle était d'autant plus gênée qu'elle ne parlait pas un mot de français.","- J'ai très faim ! -Tant mieux !"],
        "answer_set": ["The more I sleep, the more tired I feel.","The more books i read, the more i forget.","The higher you climb, the more dangerous it is.","The more shoes I buy, the more I want to buy.","The more I listen, the less I understand.","The less I see her, the less I want to see her.","I was all the more surprised because(/as/since) he didn't say anything.","She was all the more embarrassed because(/as/since) she didn't speak a word of French.","- I'm very hungry! -All the better !"]
    },
    88: {
        "train_set": ["Qui est le plus âgé dans votre famille ?", "Les leçons sont plus intéressantes quand on a l'occasion de parler.", "Qui a la voix la plus forte ?", "Le temps est le plus chaud en août."],
        "answer_set": ["Who's the oldest in your family?", "The lessons are most interesting when we get a chance to talk.", "Who has got the loudest voice?", "The weather is hottest in August."]
    },
    89: {
        "train_set": ["La plus belle fille du village.","La montagne la plus haute du monde.","Le plus grand écrivain du siècle.","Le garçon le plus gentil de la classe.","La boutique la plus chère de la ville.","Le meilleur moment du film."],
        "answer_set": ["The most beautiful girl in the village.","The highest mountain in the world.","The greatest writer of the century.","the nicest boy in the class.","The most expensive shop in town.","the best moment of the film."]
    },
    90: {
        "train_set": ["pas de test ici"],
        "answer_set": ["no test here"]
    },
    91: { 
        "train_set": ["nous irions","nous irons","elle n'aura pas","il aurait","je viendrais","je viendrai","tu parleras","tu parlerais","je saurai","je ne saurais pas"],
        "answer_set": ["we would go","we will go","she won't have","he would have","I would come","I will come","you will talk","you would come","I'll know","I would know"]
    },
    92: {
        "train_set": ["Je savais qu'il viendrait.","Elle croyait que je ne comprendrais pas.","Si j'avais le temps, j'irais avec toi.","Que ferais-tu si je n'étais pas là.","Je trouve que tu pourrais venir me voir plus souvent.","Ils devraient moins dépenser."],
        "answer_set": ["I know he woud(/'d) come.", "She tought I wouldn't understand.", "If I had time, I would(/'d) go with you.", "What would you do if I wasn't here?","I think you could come and see me more often. / I think you could come to see me more often.","They should spend less."]
    },
    93: {
        "train_set": ["j'aurais demandé","il serait allé","ma mère aurait su","personne n'aurait entendu","j'aurais oublié","elle ne serait pas tombée"],
        "answer_set": ["I would(/'d) have asked","he would have gone","my mother would have known","nobody would have heard","I would have forgotten","she wouldn't have fallen"]
    },
    94: {
        "train_set": ["Je suis fatigué.", "Quelle heure est-il ?", "Nous sommes heureux.", "Tu te trompes.", "Ils sont en retard.", "J'avais oublié que tu venais.", "J'aimerais te revoir.", "Je t'appellerai.", "Je crois qu'il va pleuvoir ce soir.", "Je ne te le dirai pas.", "Je ne les aime pas.", "Il ne sait pas."],
        "answer_set": ["I'm tired.","What's the time?","We're happy.","You're quite wrong.","They're late.","I'd forgotten you were coming.","I'd like to see you again.","I'll ring you.","I think it'll rain tonight.","I won't tell you.","I don't like them.","He doesn't know."]
    },
    95: {
        "train_set": ["Il ne peut pas nous entendre. Nous devons crier.", "Elle a pleuré pendant des jours après que son ami l'ait quittée.", "Quand nous avons vu le tigre, elle a crié de panique.", "Pourquoi les officiers crient-ils toujours aux soldats ?"],
        "answer_set": ["He can't hear us. We'll have to shout.","She cried for days after her boy(friend left her.","When we saw the tiger, she screamed in panic.","Why do officers always shout at soldiers?"]
    },
    96: {
        "train_set": ["- Demande-lui où il habite. - Je n'ose pas.","Elle n'ose pas dire à ses parents qu'elle est enceinte.","Je n'ai pas osé inviter Jim à danser.","Est-ce que tu oserais chanter devant des centaines de gens?","elle a osé demander un augmentations de salaire."],
        "answer_set": ["- Ask him where he lives - I daren't / I don't dare.","She daren't tell her parents that she's pregnant.","I didn't dare to invite Jim to dance.","Would you have the courage to sing in front of hundreds of people?","She wasn't afraid to ask a rise. /  She had to courage to ask for a rise."]
    },
    97: {
        "train_set": ["pas de test ici"],
        "answer_set": ["no test here"]
    },
    98: {
        "train_set": ["Le président est mort hier soir.", "Il est mort à l'hôpital.", "Ma mère est morte.", "Elle est morte en 1984.", "Tous ses amis sont morts maintenant.", "Plusieurs sont morts d'un cancer.", "La police a trouvé un mort dans le jardin.", "Les morts ne connaissent pas leur bonheur.", "La mort ne me fait pas peur.", "Je n'aime pas voir des morts, même au cinéma.", "une bombe a explosé ce matin, il y a eu des morts.", "L'accident a fait cinq morts."],
        "answer_set": ["The president died last night.", "He died in the hospital.", "My mother is dead.", "She died in 1984.", "All his friends are dead now.", "Many people (have) died of cancer.", "The police found a dead man in the garden.", "The dead (people) do not know their happiness.", "Death does not scare me.", "I do not like to see dead people, even in the cinema.", "A bomb exploded this morning, there were some dead people.", "Five people died (were killed) in the crash."],
    },
    99: {
        "train_set": ["- Tu viens Bob - Non, j'ai déjà mangé.", "Il est déjà dix heures.", "Avez-vous déjà rencontré ma mère?", "Est-ce que le courrier est déjà arrivé?","J'ai déjà vu ce film (je suis en train de le revoir).","je suis déjà fatigué.","Avez-vous déjà été hospitalisé?","Est-ce que Patricia a déjà téléphoné?","- Vous avez déjà terminé? - Vous mangez vite !","Quelle est votre adresse déjà?"],
        "answer_set": ["- Are you coming, bob? - No, I've already eaten.", "It's already ten.", "Have you ever met my mother?", "Has the post arrived(/come) yet?","I've seen this film before.","I'm already tired.","Have you ever been in hospital?","Has Patricia phoned yet?","-You've finished already? - You eat fast(/quickly)!","What's your address again?"]
    },
    100: {
        "train_set": ["depuis trois semaines", "depuis six mois", "depuis samedi", "depuis la guerre", "depuis longtemps", "depuis le 4 septembre", "depuis 2010", "depuis ce matin", "Il pleut depuis trois semaines.", "Elle était à Londres depuis dimanche.", "Il l'a aimée dès qu'il l'a vue.", "Je t'aime depuis que je t'ai vue.", "Nous avons marché longtemps et nous étions fatigués.", "Il y a eu des guerres depuis le début de l'histoire.", "Quand elle est arrivée, j'attendais depuis deux heures.", "J'ai aimé ce travail dès le début."],
        "answer_set": ["for three weeks", "for six months", "since Satudrday", "since the war", "for a long time", "since 4th September", "since 2010", "since this morning", "It's been raining for three weeks.", "she had been staying in London since Sunday.", "He loved her from the moment he saw her.", "I've loved you since the moment I saw you.", "We had been walking for a long time, and we were tired.", "Since the beginning of history there have been wars.", "When she arrived i had been waiting for two hours.", "I liked the job from the beginning."]
    },
    101: {
        "train_set": [""],
        "answer_set": [""]
    },
    102: { "train_set": [""], "answer_set": [""] },
    103: { "train_set": [""], "answer_set": [""] },
    104: { "train_set": [""], "answer_set": [""] },
    105: { "train_set": [""], "answer_set": [""] },
    106: { "train_set": [""], "answer_set": [""] },
    107: { "train_set": [""], "answer_set": [""] },
    108: { "train_set": [""], "answer_set": [""] },
    109: { "train_set": [""], "answer_set": [""] },
    110: { "train_set": [""], "answer_set": [""] },
    111: { "train_set": [""], "answer_set": [""] },
    112: { "train_set": [""], "answer_set": [""] },
    113: { "train_set": [""], "answer_set": [""] },
    114: { "train_set": [""], "answer_set": [""] },
    115: { "train_set": [""], "answer_set": [""] },
    116: { "train_set": [""], "answer_set": [""] },
    117: { "train_set": [""], "answer_set": [""] },
    118: { "train_set": [""], "answer_set": [""] },
    119: { "train_set": [""], "answer_set": [""] },
    120: { "train_set": [""], "answer_set": [""] },
    121: { "train_set": [""], "answer_set": [""] },
    122: { "train_set": [""], "answer_set": [""] },
    123: { "train_set": [""], "answer_set": [""] },
    124: { "train_set": [""], "answer_set": [""] },
    125: { "train_set": [""], "answer_set": [""] },
    126: { "train_set": [""], "answer_set": [""] },
    127: { "train_set": [""], "answer_set": [""] },
    128: {
        "train_set": ["Il n'est même pas dix heures.","Elle a même peur des chats.","Il aime même le latin.","Je n'ai même pas un euro.","Il parle même le tibétain.","J'irai au cinéma même si tu ne viens pas.","Je pense même que tu as raison.","Elle est encore plus belle qu'avant."],
        "answer_set": ["It's not even ten o'clock.","She's even afraid of cats.","He even likes Latin.","I don't even have one euro.","He even speaks Tibetan.","I'll go to the cinema even if you aren't coming.","I even think you're right.","She's even more beautiful than before."]
    },
    129: { "train_set": [""], "answer_set": [""] },
    130: { "train_set": [""], "answer_set": [""] },
    131: { "train_set": [""], "answer_set": [""] },
    132: { "train_set": [""], "answer_set": [""] },
    133: { "train_set": [""], "answer_set": [""] },
    134: { "train_set": [""], "answer_set": [""] },
    135: { "train_set": [""], "answer_set": [""] },
    136: { "train_set": [""], "answer_set": [""] },
    137: { "train_set": [""], "answer_set": [""] },
    138: { "train_set": [""], "answer_set": [""] },
    139: { "train_set": [""], "answer_set": [""] },
    140: { "train_set": [""], "answer_set": [""] },
    141: { "train_set": [""], "answer_set": [""] },
    142: { "train_set": [""], "answer_set": [""] },
    143: { "train_set": [""], "answer_set": [""] },
    144: { "train_set": [""], "answer_set": [""] },
    145: { "train_set": [""], "answer_set": [""] },
    146: { "train_set": [""], "answer_set": [""] },
    147: { "train_set": [""], "answer_set": [""] },
    148: { "train_set": [""], "answer_set": [""] },
    149: { "train_set": [""], "answer_set": [""] },
    150: { "train_set": [""], "answer_set": [""] },
    151: { "train_set": [""], "answer_set": [""] },
    152: { "train_set": [""], "answer_set": [""] },
    153: { "train_set": [""], "answer_set": [""] },
    154: { "train_set": [""], "answer_set": [""] },
    155: { "train_set": [""], "answer_set": [""] },
    156: { "train_set": [""], "answer_set": [""] },
    157: { "train_set": [""], "answer_set": [""] },
    158: { "train_set": [""], "answer_set": [""] },
    159: { "train_set": [""], "answer_set": [""] },
    160: { "train_set": [""], "answer_set": [""] },
    161: { "train_set": [""], "answer_set": [""] },
    162: { "train_set": [""], "answer_set": [""] },
    163: { "train_set": [""], "answer_set": [""] },
    164: { "train_set": [""], "answer_set": [""] },
    165: { "train_set": [""], "answer_set": [""] },
    166: { "train_set": [""], "answer_set": [""] },
    167: { "train_set": [""], "answer_set": [""] },
    168: { "train_set": [""], "answer_set": [""] },
    169: { "train_set": [""], "answer_set": [""] },
    170: { "train_set": [""], "answer_set": [""] },
    171: { "train_set": [""], "answer_set": [""] },
    172: { "train_set": [""], "answer_set": [""] },
    173: { "train_set": [""], "answer_set": [""] },
    174: { "train_set": [""], "answer_set": [""] },
    175: { "train_set": [""], "answer_set": [""] },
    176: { "train_set": [""], "answer_set": [""] },
    177: { "train_set": [""], "answer_set": [""] },
    178: { "train_set": [""], "answer_set": [""] },
    179: { "train_set": [""], "answer_set": [""] },
    180: { "train_set": [""], "answer_set": [""] },
    181: { "train_set": [""], "answer_set": [""] },
    182: { "train_set": [""], "answer_set": [""] },
    183: { "train_set": [""], "answer_set": [""] },
    184: { "train_set": [""], "answer_set": [""] },
    185: { "train_set": [""], "answer_set": [""] },
    186: { "train_set": [""], "answer_set": [""] },
    187: { "train_set": [""], "answer_set": [""] },
    188: { "train_set": [""], "answer_set": [""] },
    189: { "train_set": [""], "answer_set": [""] },
    190: { "train_set": [""], "answer_set": [""] },
    191: { "train_set": [""], "answer_set": [""] },
    192: { "train_set": [""], "answer_set": [""] },
    193: { "train_set": [""], "answer_set": [""] },
    194: { "train_set": [""], "answer_set": [""] },
    195: { "train_set": [""], "answer_set": [""] },
    196: { "train_set": [""], "answer_set": [""] },
    197: { "train_set": [""], "answer_set": [""] },
    198: { "train_set": [""], "answer_set": [""] },
    199: { "train_set": [""], "answer_set": [""] },
    200: { "train_set": [""], "answer_set": [""] },
    201: { "train_set": [""], "answer_set": [""] },
    202: { "train_set": [""], "answer_set": [""] },
    203: { "train_set": [""], "answer_set": [""] },
    204: { "train_set": [""], "answer_set": [""] },
    205: { "train_set": [""], "answer_set": [""] },
    206: { "train_set": [""], "answer_set": [""] },
    207: { "train_set": [""], "answer_set": [""] },
    208: { "train_set": [""], "answer_set": [""] },
    209: { "train_set": [""], "answer_set": [""] },
    210: { "train_set": [""], "answer_set": [""] },
    211: { "train_set": [""], "answer_set": [""] },
    212: { "train_set": [""], "answer_set": [""] },
    213: { "train_set": [""], "answer_set": [""] },
    214: { "train_set": [""], "answer_set": [""] },
    215: { "train_set": [""], "answer_set": [""] },
    216: { "train_set": [""], "answer_set": [""] },
    217: { "train_set": [""], "answer_set": [""] },
    218: { "train_set": [""], "answer_set": [""] },
    219: { "train_set": [""], "answer_set": [""] },
    220: { "train_set": [""], "answer_set": [""] },
    221: { "train_set": [""], "answer_set": [""] },
    222: { "train_set": [""], "answer_set": [""] },
    223: { "train_set": [""], "answer_set": [""] },
    224: { "train_set": [""], "answer_set": [""] },
    225: { "train_set": [""], "answer_set": [""] },
    226: { "train_set": [""], "answer_set": [""] },
    227: { "train_set": [""], "answer_set": [""] },
    228: { "train_set": [""], "answer_set": [""] },
    229: { "train_set": [""], "answer_set": [""] },
    230: { "train_set": [""], "answer_set": [""] },
    231: { "train_set": [""], "answer_set": [""] },
    232: { "train_set": [""], "answer_set": [""] },
    233: { "train_set": [""], "answer_set": [""] },
    234: { "train_set": [""], "answer_set": [""] },
    235: { "train_set": [""], "answer_set": [""] },
    236: { "train_set": [""], "answer_set": [""] },
    237: { "train_set": [""], "answer_set": [""] },
    238: { "train_set": [""], "answer_set": [""] },
    239: { "train_set": [""], "answer_set": [""] },
    240: { "train_set": [""], "answer_set": [""] },
    241: { "train_set": [""], "answer_set": [""] },
    242: { "train_set": [""], "answer_set": [""] },
    243: { "train_set": [""], "answer_set": [""] },
    244: { "train_set": [""], "answer_set": [""] },
    245: { "train_set": [""], "answer_set": [""] },
    246: { "train_set": [""], "answer_set": [""] },
    247: { "train_set": [""], "answer_set": [""] },
    248: { "train_set": [""], "answer_set": [""] },
    249: { "train_set": [""], "answer_set": [""] },
    250: { "train_set": [""], "answer_set": [""] },
    251: { "train_set": [""], "answer_set": [""] },
    252: { "train_set": [""], "answer_set": [""] },
    253: { "train_set": [""], "answer_set": [""] },
    254: { "train_set": [""], "answer_set": [""] },
    255: { "train_set": [""], "answer_set": [""] },
    256: { "train_set": [""], "answer_set": [""] },
    257: { "train_set": [""], "answer_set": [""] },
    258: { "train_set": [""], "answer_set": [""] },
    259: { "train_set": [""], "answer_set": [""] },
    260: { "train_set": [""], "answer_set": [""] },
    261: { "train_set": [""], "answer_set": [""] },
    262: { "train_set": [""], "answer_set": [""] },
    263: { "train_set": [""], "answer_set": [""] },
    264: { "train_set": [""], "answer_set": [""] },
    265: { "train_set": [""], "answer_set": [""] },
    266: { "train_set": [""], "answer_set": [""] },
    267: { "train_set": [""], "answer_set": [""] },
    268: { "train_set": [""], "answer_set": [""] },
    269: { "train_set": [""], "answer_set": [""] },
    270: { "train_set": [""], "answer_set": [""] },
    271: { "train_set": [""], "answer_set": [""] },
    272: { "train_set": [""], "answer_set": [""] },
    273: { "train_set": [""], "answer_set": [""] },
    274: { "train_set": [""], "answer_set": [""] },
    275: { "train_set": [""], "answer_set": [""] },
    276: { "train_set": [""], "answer_set": [""] },
    277: { "train_set": [""], "answer_set": [""] },
    278: { "train_set": [""], "answer_set": [""] },
    279: { "train_set": [""], "answer_set": [""] },
    280: { "train_set": [""], "answer_set": [""] },
    281: { "train_set": [""], "answer_set": [""] },
    282: { "train_set": [""], "answer_set": [""] },
    283: { "train_set": [""], "answer_set": [""] },
    284: { "train_set": [""], "answer_set": [""] },
    285: { "train_set": [""], "answer_set": [""] },
    286: { "train_set": [""], "answer_set": [""] },
    287: { "train_set": [""], "answer_set": [""] },
    288: { "train_set": [""], "answer_set": [""] },
    289: { "train_set": [""], "answer_set": [""] },
    290: { "train_set": [""], "answer_set": [""] },
    291: { "train_set": [""], "answer_set": [""] },
    292: { "train_set": [""], "answer_set": [""] },
    293: { "train_set": [""], "answer_set": [""] },
    294: { "train_set": [""], "answer_set": [""] },
    295: { "train_set": [""], "answer_set": [""] },
    296: { "train_set": [""], "answer_set": [""] },
    297: { "train_set": [""], "answer_set": [""] },
    298: { "train_set": [""], "answer_set": [""] },
    299: { "train_set": [""], "answer_set": [""] },
    300: { "train_set": [""], "answer_set": [""] },
    301: { "train_set": [""], "answer_set": [""] },
    302: { "train_set": [""], "answer_set": [""] },
    303: { "train_set": [""], "answer_set": [""] },
    304: { "train_set": [""], "answer_set": [""] },
    305: { "train_set": [""], "answer_set": [""] },
    306: { "train_set": [""], "answer_set": [""] },
    307: { "train_set": [""], "answer_set": [""] },
    308: { "train_set": [""], "answer_set": [""] },
    309: { "train_set": [""], "answer_set": [""] },
    310: { "train_set": [""], "answer_set": [""] },
    311: { "train_set": [""], "answer_set": [""] },
    312: { "train_set": [""], "answer_set": [""] },
    313: { "train_set": [""], "answer_set": [""] },
    314: { "train_set": [""], "answer_set": [""] },
    315: { "train_set": [""], "answer_set": [""] },
    316: { "train_set": [""], "answer_set": [""] },
    317: { "train_set": [""], "answer_set": [""] },
    318: { "train_set": [""], "answer_set": [""] },
    319: { "train_set": [""], "answer_set": [""] },
    320: { "train_set": [""], "answer_set": [""] },
    321: { "train_set": [""], "answer_set": [""] },
    322: { "train_set": [""], "answer_set": [""] },
    323: { "train_set": [""], "answer_set": [""] },
    324: { "train_set": [""], "answer_set": [""] },
    325: { "train_set": [""], "answer_set": [""] },
    326: { "train_set": [""], "answer_set": [""] },
    327: { "train_set": [""], "answer_set": [""] },
    328: { "train_set": [""], "answer_set": [""] },
    329: { "train_set": [""], "answer_set": [""] },
    330: { "train_set": [""], "answer_set": [""] },
    331: { "train_set": [""], "answer_set": [""] },
    332: { "train_set": [""], "answer_set": [""] },
    333: { "train_set": [""], "answer_set": [""] },
    334: { "train_set": [""], "answer_set": [""] },
    335: { "train_set": [""], "answer_set": [""] },
    336: { "train_set": [""], "answer_set": [""] },
    337: { "train_set": [""], "answer_set": [""] },
    338: { "train_set": [""], "answer_set": [""] },
    339: { "train_set": [""], "answer_set": [""] },
    340: { "train_set": [""], "answer_set": [""] },
    341: { "train_set": [""], "answer_set": [""] },
    342: { "train_set": [""], "answer_set": [""] },
    343: { "train_set": [""], "answer_set": [""] },
    344: { "train_set": [""], "answer_set": [""] },
    345: { "train_set": [""], "answer_set": [""] },
    346: { "train_set": [""], "answer_set": [""] },
    347: { "train_set": [""], "answer_set": [""] },
    348: { "train_set": [""], "answer_set": [""] },
    349: { "train_set": [""], "answer_set": [""] },
    350: { "train_set": [""], "answer_set": [""] },
    351: { "train_set": [""], "answer_set": [""] },
    352: { "train_set": [""], "answer_set": [""] },
    353: { "train_set": [""], "answer_set": [""] },
    354: { "train_set": [""], "answer_set": [""] },
    355: { "train_set": [""], "answer_set": [""] },
    356: { "train_set": [""], "answer_set": [""] },
    357: { "train_set": [""], "answer_set": [""] },
    358: { "train_set": [""], "answer_set": [""] },
    359: { "train_set": [""], "answer_set": [""] },
    360: { "train_set": [""], "answer_set": [""] },
    361: { "train_set": [""], "answer_set": [""] },
    362: { "train_set": [""], "answer_set": [""] },
    363: { "train_set": [""], "answer_set": [""] },
    364: { "train_set": [""], "answer_set": [""] },
    365: { "train_set": [""], "answer_set": [""] },
    366: { "train_set": [""], "answer_set": [""] },
    367: { "train_set": [""], "answer_set": [""] },
    368: { "train_set": [""], "answer_set": [""] },
    369: { "train_set": [""], "answer_set": [""] },
    370: { "train_set": [""], "answer_set": [""] },
    371: { "train_set": [""], "answer_set": [""] },
    372: { "train_set": [""], "answer_set": [""] },
    373: { "train_set": [""], "answer_set": [""] },
    374: { "train_set": [""], "answer_set": [""] },
    375: { "train_set": [""], "answer_set": [""] },
    376: { "train_set": [""], "answer_set": [""] },
    377: { "train_set": [""], "answer_set": [""] },
    378: { "train_set": [""], "answer_set": [""] },
    379: { "train_set": [""], "answer_set": [""] },
    380: { "train_set": [""], "answer_set": [""] },
    381: { "train_set": [""], "answer_set": [""] },
    382: { "train_set": [""], "answer_set": [""] },
    383: { "train_set": [""], "answer_set": [""] },
    384: { "train_set": [""], "answer_set": [""] },
    385: { "train_set": [""], "answer_set": [""] },
    386: { "train_set": [""], "answer_set": [""] },
    387: { "train_set": [""], "answer_set": [""] },
    388: { "train_set": [""], "answer_set": [""] },
    389: { "train_set": [""], "answer_set": [""] },
    390: { "train_set": [""], "answer_set": [""] },
    391: { "train_set": [""], "answer_set": [""] },
    392: { "train_set": [""], "answer_set": [""] },
    393: { "train_set": [""], "answer_set": [""] },
    394: { "train_set": [""], "answer_set": [""] },
    395: { "train_set": [""], "answer_set": [""] },
    396: { "train_set": [""], "answer_set": [""] },
    397: { "train_set": [""], "answer_set": [""] },
    398: { "train_set": [""], "answer_set": [""] },
    399: { "train_set": [""], "answer_set": [""] },
    400: { "train_set": [""], "answer_set": [""] },
    401: { "train_set": [""], "answer_set": [""] },
    402: { "train_set": [""], "answer_set": [""] },
    403: { "train_set": [""], "answer_set": [""] },
    404: { "train_set": [""], "answer_set": [""] },
    405: { "train_set": [""], "answer_set": [""] },
    406: { "train_set": [""], "answer_set": [""] },
    407: { "train_set": [""], "answer_set": [""] },
    408: { "train_set": [""], "answer_set": [""] },
    409: { "train_set": [""], "answer_set": [""] },
    410: { "train_set": [""], "answer_set": [""] },
    411: { "train_set": [""], "answer_set": [""] },
    412: { "train_set": [""], "answer_set": [""] },
    413: { "train_set": [""], "answer_set": [""] },
    414: { "train_set": [""], "answer_set": [""] },
    415: { "train_set": [""], "answer_set": [""] },
    416: { "train_set": [""], "answer_set": [""] },
    417: {
        "train_set": [""],
        "answer_set": ["Katie's looking for a new job.","In the North-East, it's very difficult to find work.","They're playing Schubert's complete works on the radio this month.","I've got an interesting job."]
    },
    418: {
        "train_set": ["Elle ne vaut pas la peine qu'on lui parle.","Cela ne vaut pas la peine de lui parler", "Cela vaut la peine de voir Venise.","Venise vaut la peine d'être vue", "Ses idées ne valent pas la peine d'être écoutées.","Cela ne vaut pas la peine d'écouter ses idées", "Cela vaut la peine de visiter les Cornouailles", "Les Cornouailles valent la peine d'être visitées", "Son dernier livre ne vaut pas la peine d'être lu.", "C'est pas la peine de lire son dernier livre", "C'est pas la peine de se disputer avec elle.","Elle ne vaut pas la peine de se disputer avec"],
        "answer_set": ["she's not worth talking to.", "It's not worth talking to her", "It's worth seeing Venice.", "Venise is worth seeing.", "His ideas are not worth listening.", "It's not worth listening to his ideas.", "It's woth visiting Cornwall.", "Cornwall is worth visiting.", "His latest book isn't worth reading.", "It's not worth reading his latest book.", "it's not worth arguing with her.","She's not worth arguing with."]
    },
    419: {
        "train_set": ["J'aimerais mieux rester chez moi.", "- Une bière? - Je préfrerais prendre un jus d'orange.", "Est-e que tu préfères voir un film ou aller au théâtre ce soir?", "Qu'est-ce que tu aimerais mieux faire?","Je préférerais que tu restes chez toi.","Elle préférerait qu'on arrête de jouer.","- Je ferme la porte? - Je préfère pas.","J'aimerais mieux que tu ne dises rien."],
        "answer_set": ["I'd rather stay at home.","- A beer? -I'd rather have an orange juice.","Would you rather see a film or go to the theatre this evening?","What would you rather do?","I'd rather you stayed at home","She'd rather we stopped playing.","- Shall I close the door? - I'd rather you didn't","I'd rather you didn't say anything. / I'd rather you said nothing."]                                     
    },
    420: {
        "train_set": ["650 grammes est 0,65 kilogrammes","Mon numéro de compte est 3046729","Température maximum 17°, température minimum 0°","L'Angleterre a gagné le match de foot 3-0"],
        "answer_set": ["Six hundred and fifty grams is nought point sixty-five kilograms.","My account number is three O four seven two nine.","Maximum temperature seventeen degrees, minimum temperature zero degrees","England won the football match tree-nil"]
    },
}


// ###############################################################################





// table des exerices et réponses
train_table = train_answers[num].train_set
answer_table = train_answers[num].answer_set

function ajoutElementTexte(texte, doc) {
    // ajoute du texte dans un élément
    var tmp = document.createElement('p');
    tmp.innerHTML = unescape(escape(texte));
    doc.appendChild(tmp);
}

for (let i = 0; i < train_table.length; i++) {
    ajoutElementTexte((i + 1) + ". \n" + train_table[i] , document.getElementById("train"));
    ajoutElementTexte((i + 1) + ". \n" +answer_table[i], document.getElementById("answers"));

}


